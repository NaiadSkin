<?php
/*
 * Copyright (c) 2011-2012 Francesco Siddi (fsiddi.com), Luca Bonavita (mindrones.com)
 *           (c) 2015      Marcin Cieślak (saper.info) 
 * 
 * This file is part of Naiad Skin for Mediawiki:
 * http://wiki.blender.org/index.php/Meta:Skins/Naiad/Mediawiki
 * 
 * Naiad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Naiad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Naiad.  If not, see <http://www.gnu.org/licenses/>. 
 */

/**
 * Naiad skin
 *
 * @file
 * @ingroup Extensions
 * @license http://www.gnu.org/copyleft/gpl.html GNU General Public License 3.0 or later
*/

if( !defined( 'MEDIAWIKI' ) )
	die();

if ( function_exists( 'wfLoadSkin' ) ) {
    $ext = preg_replace( '%^.+/(.+?)\.php$%', '$1', __FILE__ );
    die(
        "Deprecated PHP entry point used for <i>$ext</i> skin. Please use wfLoadSkin instead, " .
        'see <a href="https://www.mediawiki.org/wiki/Extension_registration">Extension registration</a> for more details.'
    );
    return;
} else {
    die(
        "<b>Fatal error:</b> This version of the <i>$ext</i> skin requires MediaWiki 1.25+, " .
        'either <a href="https://mediawiki.org/wiki/Download">upgrade your MediaWiki</a> or download the extension code from the ' .
        '<a href="https://github.com/OrganicDesign/extensions/tree/MediaWiki-1.24/MediaWiki">1.24 branch</a>.'
	);
}


