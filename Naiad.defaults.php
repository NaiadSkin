<?php

/*
 * Copyright (c) 2011-2012 Francesco Siddi (fsiddi.com), Luca Bonavita (mindrones.com)
 * Copyright (c) 2015      Marcin Cieślak (saper.info)
 * 
 * This file is part of Naiad Skin for Mediawiki:
 * http://wiki.blender.org/index.php/Meta:Skins/Naiad/Mediawiki
 * 
 * Naiad is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Naiad is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Naiad.  If not, see <http://www.gnu.org/licenses/>. 
 */

$messages = array();

$messages['en'] = array(
	'navTreeTopPages' => '',
	'navTree' => '',
	'languages' => '',
	'series' => '',
	'navigationArrows' => '',
	'sidebarBanner' => '',
	'searchLanguages' => '',
	'searchSeries' => '',
	'naiad-sidebar-key' => 'navigation',
	'naiad-navigation' => 'n-blender|n-code',
	'naiad-navigation-n-blender' => 'http://www.blender.org | blender.org | Go to blender.org website',
	'naiad-navigation-n-code' => 'http://code.blender.org | code.blender.org | Go to blender development blog'
);
